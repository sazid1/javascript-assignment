class Person {
    constructor(name, age, salary, sex) {
        this.name = name;  this.age = age;
        this.salary = salary; this.sex = sex;
    }
    static sort(arr, field, order) {
        const tempArr = arr;
        this.quickSort(tempArr, field, order, 0, tempArr.length - 1);
        return tempArr;
    }
    static quickSort(arr, field, order, start, end) {
        if (start > end) {
            return;
        }
        const pivot = arr[end][field];
        let left = start - 1;
        for (let i = start; i <= end - 1; i++) {
            if (order == 'asc') {
                if (arr[i][field] < pivot) {
                    left++;
                    this.swap(arr, left, i);
                }
            } else if (order == 'desc') {
                if (arr[i][field] > pivot) {
                    left++;
                    this.swap(arr, left, i);
                }
            }
        }
        this.swap(arr, left + 1, end);
        this.quickSort(arr, field, order, start, left);
        this.quickSort(arr, field, order, left + 2, end);
    }
    static swap(arr, i, j) {
        const temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
const obj1 = new Person("sazid", 21, 100, "male");
const obj2 = new Person("khan", 24, 500, "female");
const obj3 = new Person("aashish", 21, 50, "male");
const obj4 = new Person("negi", 22, 200, "male");
const obj = [obj1, obj2,obj3,obj4];
console.log(Person.sort(obj, 'name', 'asc'))