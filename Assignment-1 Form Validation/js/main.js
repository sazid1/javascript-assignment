function printError(Id, Msg) {
    document.getElementById(Id).innerHTML = Msg;
}

function validateForm() {

    var name = document.Form.name.value;
    var email = document.Form.email.value;
    var mobile = document.Form.mobile.value;
    var password = document.Form.password.value;
    var control = document.Form.control.value;

    var nameErr = emailErr = mobileErr  = passwordErr = controlErr = true;
    

    if(name == "") {
        printError("nameErr", "Please enter your name");
        var fullName = document.getElementById("name");
            fullName.classList.add("input-2");
            fullName.classList.remove("input-1");
    } else {
        var regex = /^[a-zA-Z\s]+$/;                
        if(regex.test(name) === false) {
            printError("nameErr", "Please enter a valid name");
            var fullName = document.getElementById("name");
            fullName.classList.add("input-2");
            fullName.classList.remove("input-1");
        } else {
            printError("nameErr", "");
            nameErr = false;
            var fullName = document.getElementById("name");
            fullName.classList.add("input-1");
            fullName.classList.remove("input-2");

            
        }
    }
    if(email == "") {
        printError("emailErr", "Please enter your email address");
         var elem = document.getElementById("email");
            elem.classList.add("input-2");
            elem.classList.remove("input-1");
    } else {
        
        var regex = /^\S+@\S+\.\S+$/;
        if(regex.test(email) === false) {
            printError("emailErr", "Please enter a valid email address");
            var elem = document.getElementById("email");
            elem.classList.add("input-2");
            elem.classList.remove("input-1");
        } else{
            printError("emailErr", "");
            emailErr = false;
             var elem = document.getElementById("email");
            elem.classList.add("input-1");
            elem.classList.remove("input-2");

        }
    }
    
    if(mobile == "") {
        printError("mobileErr", "Please enter your mobile number");
        var elem = document.getElementById("mobile");
            elem.classList.add("input-2");
            elem.classList.remove("input-1");
    } else {
        var regex = /^[1-9]\d{9}$/;
        if(regex.test(mobile) === false) {
            printError("mobileErr", "Please enter a valid 10 digit mobile number");
            var elem = document.getElementById("mobile");
            elem.classList.add("input-2");
            elem.classList.remove("input-1");
        } else{
            printError("mobileErr", "");
            mobileErr = false;
            var elem = document.getElementById("mobile");
            elem.classList.add("input-1");
            elem.classList.remove("input-2");
        }
    }
    if(password == ""){
        printError("passwordErr", "Please Enter your Password");
        var pass = document.getElementById("password");
            pass.classList.add("input-2")
            pass.classList.remove("input-1")
    }else{
        var regex = /^[a-zA-Z0-9!@#$%^&*]{6,16}$/;
        if(regex.text(password) === false){
            printError("passwordErr", "Please enter a password");
            var pass = document.getElementById('password');
            pass.classList.add("input-2");
            pass.classList.remove("input-1");
        }
         else{
            printError("passwordErr", "");
            passwordErr = false;
            var pass = document.getElementById("password");
            pass.classList.add("input-1");
            pass.classList.remove("input-2");
        }
    }
    if(control == "") {
        printError("controlErr", "Please select your control");
    } else {
        printError("controlErr", "");
        controlErr = false;
    }
    
    
    if((nameErr || emailErr || mobileErr || passwordErr || controlErr) == true) {
       return false;
    } 
};